#!/usr/bin/env python

import dpkt
import sys
import struct
import array
from datetime import datetime as dt
import socket


class TCPStatsExtract:
  """TCP Stats Extractor class"""
  def __init__(self, inputFile=None):
    """Constructor, define all object's attributes"""
    self.mInputFile            = None  # input file name
    self.mRttAvg               = 0     # Average RTT
    self.mRttList              = []    # List of RTTs
    self.mSrcPort              = 0     # Src port
    self.mDstPort              = 0     # Dst port
    self.mSrcIP                = None  # Src IP
    self.mDstIP                = None  # Dst IP
    self.mWinScaleFactorSrc    = 0     # Window scale factor src
    self.mWinScaleFactorDst    = 0     # Window scale factor dst
    self.mMssSrc               = 0     # Src MSS
    self.mMssDst               = 0     # Dst MSS
    self.mWinSizesList         = []    # All window sizes
    self.mWinSizesListSrc      = []    # Window sizes src
    self.mWinSizesListDst      = []    # Window sizes dst
    self.mWinCalcSizesList     = []    # All calculated window sizes
    self.mCalcWinSizesListSrc  = []    # Calculated window sizes src
    self.mCalcWinSizesListDst  = []    # Calculated window sizes dst
    self.mTimestampList        = []    # List of all timestamps
    self.mTransferedDataSum    = 0     # Total amount of transfered data
    self.mTransferedTCPDataSum = 0     # Total amount of transfered TCP data
    self.mThroughputList       = []    # List of throughputs per stream
    self.mTransmitedSeqList    = []    # List of transmited sequentials numbers (e.g. for slow-start)
    self.mTransmitedSeqTsList  = []    # List of timestamps related to transmited sequential numbers
    self.mTotalFrames          = 0
    if inputFile != None:
      self.mInputFile = inputFile

  def SetInputFile(self, inputFile):
    """Set input file name (pcap file)"""
    self.mInputFile = inputFile

  def _GetTCPFlags(self, flags):
    """Tranform flags number from TCP packet to list of specific flags"""
    flags_list = ['F', 'S', 'R', 'P', 'A', 'U']
    res_flags = []
    mask = 0b1

    for i in range(6):
      if mask & flags:
        res_flags.append(flags_list[i])
      mask <<= 1

    return res_flags

  def _CalculateWinScaleFactor(self, opts):
    """Get window scaling factor and MSS from tcp opts"""
    win_scale = 0
    mss       = 0
    # if we have some option, process it
    if len(opts) > 0:
      tcp_opts = dpkt.tcp.parse_opts(opts)
      for opt in tcp_opts:
        # Option 3 - Win scale factor
        try:
          if opt[0] == 3:
            win_scale = int(opt[1].encode("hex"), 16)
          elif opt[0] == 2:
            mss = int(opt[1].encode("hex"), 16)
        except:
          print("Error: _CalculateWinScaleFactor()")
    return win_scale, mss

  def _CalculateWinSize(self, win, port):
    """Calculate window size (we consider win scale factor) of one side,
       append to specific list by src/dst port"""
    calc_win_size = 0

    if port == self.mSrcPort:
      calc_win_size = win << self.mWinScaleFactorSrc
      self.mCalcWinSizesListSrc.append(calc_win_size)
    else:
      calc_win_size = win << self.mWinScaleFactorDst
      self.mCalcWinSizesListDst.append(calc_win_size)

    self.mWinCalcSizesList.append(calc_win_size)
    return calc_win_size

  def _StoreWinSize(self, win, port):
    """Append window size to specific lists of sizes"""
    if port == self.mSrcPort:
      self.mWinSizesListSrc.append(win)
    else:
      self.mWinSizesListDst.append(win)

    self.mWinSizesList.append(win)

  def _GetTimestampDiff(self, ts1, ts2):
    """Calculate difference between two timestamps in microseconds"""
    dt_start = dt.fromtimestamp(ts1)
    dt_end   = dt.fromtimestamp(ts2)
    dt_diff  = (dt_end - dt_start).total_seconds() * 1000000

    return dt_diff

  def ReadPcap(self, inputFile=None):
    """Read pcap file and process it"""
    if inputFile != None:
      self.mInputFile = inputFile

    frame     = 1
    rtt_dict  = {}
    rtt_sum   = 0
    timestamp_first      = 0
    seq_first            = 0
    
    with open(self.mInputFile, "r") as f:
      pcap = dpkt.pcap.Reader(f)

      for timestamp, buf in pcap:
        # check if there is ethernet, retrieve data
        eth = dpkt.ethernet.Ethernet(buf)
        if eth.type != dpkt.ethernet.ETH_TYPE_IP:
          continue
        # check if there is TCP, retrieve data
        ip = eth.data
        if ip.p != dpkt.ip.IP_PROTO_TCP:
          continue
        # get TCP data
        tcp = ip.data
        # retrieve TCP flags from numbers to human readable form
        tcp_flags = self._GetTCPFlags(tcp.flags)
        
        # first frame - store src/dst port
        if frame == 1:
          self.mSrcPort   = tcp.sport
          self.mDstPort   = tcp.dport
          self.mSrcIP     = socket.inet_ntop(socket.AF_INET, ip.src)
          self.mDstIP     = socket.inet_ntop(socket.AF_INET, ip.dst)
          self.mTransferedDataSum = len(buf)
          timestamp_first = timestamp
          self.mThroughputList.append(0)
          seq_first = tcp.seq
        else:
          self.mTransferedDataSum += len(buf)
          time_delta = timestamp - timestamp_first
          self.mThroughputList.append(self.mTransferedDataSum / time_delta);

        # if SYN packet was recieved, check if it has win scale option (3)
        # TODO: compute only one time ? (win scale factor is setted only one)
        if 'S' in tcp_flags:
          if tcp.sport == self.mSrcPort:
            self.mWinScaleFactorSrc, self.mMssSrc = self._CalculateWinScaleFactor(tcp.opts)
          else:
            self.mWinScaleFactorDst, self.mMssDst = self._CalculateWinScaleFactor(tcp.opts)
          #print(frame, tcp.win)
        else:
          # TODO: if both scale factor are 0, what to do ?
          # TODO: or when scale factor are not supported? check it!
          #if self.mWinScaleFactorSrc != 0 and self.mWinScaleFactorDst != 0:
          win_size = self._CalculateWinSize(tcp.win, tcp.sport)

        # store origina window size by port
        self._StoreWinSize(tcp.win, tcp.sport)

        # store sequential number and timestamp for slow-start algorithm e.g. (sender)
        if tcp.sport == self.mSrcPort:
          if ('S' not in tcp_flags) and ('F' not in tcp_flags):
            self.mTransmitedSeqList.append(tcp.seq)
            self.mTransmitedSeqTsList.append(timestamp)

        # if we recieve ACK, calculate RTT
        # RTT: time between time stamps of next_seq_number and
        # packet with ack == next_seq_number
        if 'A' in tcp_flags:
          if tcp.ack in rtt_dict:
            rtt = timestamp - rtt_dict[tcp.ack]
            del rtt_dict[tcp.ack]
            self.mRttList.append("%.9f" % rtt)
            rtt_sum += rtt

        # if payload is bigger then 0, calculate next_seq_number as actual seq + payload
        # else next_seq_number = actual seq + 1
        if len(tcp.data) > 0:
          next_seq_number = tcp.seq + len(tcp.data)
          rtt_dict[next_seq_number] = timestamp
          self.mTransferedTCPDataSum += len(tcp.data)
        else:
          next_seq_number = tcp.seq + 1
          rtt_dict[next_seq_number] = timestamp

        #store time stamp
        self.mTimestampList.append(timestamp)

        frame += 1
        self.mTotalFrames += 1

  def PrintWindowSizes(self):
    """Print window sizes in format: origin, calculated"""
    delta = len(self.mWinSizesList) - len(self.mWinCalcSizesList)
    for idx, item in enumerate(self.mWinSizesList):
      if idx < delta:
        print(idx+1, item, 0)
      else:
        print(idx+1, item, self.mWinCalcSizesList[idx-delta])

  def GetTransferDuration(self):
    """Calculate total time of TCP stream in microseconds"""
    return self._GetTimestampDiff(self.mTimestampList[0], self.mTimestampList[-1])
    
  def GetRTTsList(self):
    """Return list of RTTs (both sides)"""
    return self.mRttList

  def GetRTTAvg(self):
    """Get average round trip time of TCP stream"""
    if len(self.mRttList) > 0:
      rtt_sum = 0
      for rtt in self.mRttList:
        rtt_sum += float(rtt)

      self.mRttAvg = "%.6f" % (rtt_sum / len(self.mRttList))

    return self.mRttAvg

  def GetWinSizesList(self):
    """Return list of windows sizes (both sides)"""
    return self.mWinSizesList

  def GetCalcWinSizesList(self):
    """Return list of calculated windows sizes (both sides)"""
    return self.mCalcWinSizesList

  def GetCalcWinSizesListSrc(self):
    """Return list of calculated windows sizes (src)"""
    return self.mCalcWinSizesListSrc

  def GetCalcWinSizesListDst(self):
    """Return list of calculated windows sizes (dst)"""
    return self.mCalcWinSizesListDst

  def GetThroughputsList(self):
    """Return list of throughputs (both sides)"""
    return self.mThroughputList

  def GetTotalDataSum(self):
    """Return sum of total data transfered (both sides)"""
    return self.mTransferedDataSum

  def GetTransmitedSeqTsLists(self):
    """Return two lists with sequential numbers and
       timestamps related to each sequential number"""
    return self.mTransmitedSeqList, self.mTransmitedSeqTsList

  def GetTCPSummaryStatistics(self):
    """Return"""
    info_dict = {}
    info_dict["src_port"]       = self.mSrcPort
    info_dict["dst_port"]       = self.mDstPort
    info_dict["src_ip"]         = self.mSrcIP
    info_dict["dst_ip"]         = self.mDstIP
    info_dict["total_frames"]   = self.mTotalFrames
    info_dict["total_data"]     = self.mTransferedDataSum / 1024
    info_dict["total_tcp_data"] = self.mTransferedTCPDataSum / 1024
    info_dict["src_win_scale"]  = self.mWinScaleFactorSrc
    info_dict["dst_win_scale"]  = self.mWinScaleFactorDst
    info_dict["src_mss"]        = self.mMssSrc
    info_dict["dst_mss"]        = self.mMssDst
    
    return info_dict

if __name__ == "__main__":

  if len(sys.argv) < 2:
    print("ARGS !")
    sys.exit(1)

  input_file = sys.argv[1]

  tce = TCPStatsExtract()
  tce.ReadPcap(input_file)
  print(tce.GetRTTAvg())
  print(tce.GetTransferDuration())
