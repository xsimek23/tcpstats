#!/usr/bin/env bash

# update
sudo apt-get update -y
# install dpkt (parsing pcaps)
sudo apt-get install -y python-dpkt
# install matplotlib (charts)
sudo apt-get install -y python-matplotlib

